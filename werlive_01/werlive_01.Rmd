---
title: "We R Live 01: Por onde começaR?<br><br>"
subtitle: "<br>GeoCastBrasil"
author: "Maurício Vancine <br> Felipe Sodré M. Barros <br>"
date: "<br> 28/04/2019"
output:
  xaringan::moon_reader:
    css: [metropolis]
    lib_dir: libs
    nature:
      highlightStyle: rainbow
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, encoding = "UTF-8")
```

# We R Live 01: Por onde começaR?

## Tópicos
### 1. Ideia das lives de R - We R Live
### 2. Papo inicial
### 3. Instalando o R
### 4. Instalando o RStudio
### 5. Como o RStudio funciona?
### 6. Pacotes
### 7. Instalando nosso primeiro pacote
### 8. Material (mas não muito)

---

class: inverse, middle, center

# 1. Ideia das lives de R - We R Live

---

background-image: url(img/logo_werlive.png)
background-size: 300px
background-position: 95% 40%

# 1. Ideia das lives de R - We R Live

## Vamos começar à falar de R e ir seguindo...

<br>

1. Manejo de dados e análises geoespaciais
2. Sensoriamento Remoto (RS)
3. Modelos de Distribuição de Espécies (SDMs)
4. tidyverse
5. R Markdown

--

<br>

### Sugestões de temas são bem-vindas!

--

<br>

### **O desafio**: fazer lives de 1 hora e sempre com um tema geoespacial

---

class: clear, inverse, middle, center

# 2. Papo inicial

---

background-image: url(img/r_logo.svg)
background-size: 200px
background-position: 90% 10%

# 2. Papo inicial

## Por que usar o R? 

### 1. É **grátis**!

--

### 2. É **"user-friendly"**!

--

### 3. Implementação de **rotinas** (repetir várias operações)!

--

### 4. Faz **gráficos** de forma eficiente

--

### 5. É considerada uma das **principais linguagens** para análise de dados

<br>

[*] https://insights.stackoverflow.com/survey/2019#technology

---

# 2. Papo inicial

<br>

## O que você pode fazer com o R?

<br><br>

--


### ...ou...

<br><br>

--

## **Como você pode fazer algo com o R?**

---

class: clear, inverse, middle, center

# Qual nossa história com o R?

---

background-image: url(img/principio_agora_pt.png)
background-size: 400px
background-position: 50% 50%

# 2. Papo inicial

## Nossa história com o R

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

Fonte: [@allison_horst](https://twitter.com/allison_horst)

---

class: clear, inverse, middle, center

# E de onde surgiu o R?

---

background-image: url(img/person_john_chambers.jpg)
background-size: 300px
background-position: 80% 80%

# 2. Papo inicial

## Histórico - Linguagem S

### John M. Chambers (Universidade de Stanford, CA, EUA)

- Old S (1976-1987)
- New S (1988-1997)
- S4 (1998)
<br><br>
- Interface: S-PLUS (1988-2008)

---

background-image: url(img/person_gentleman_ihaka.jpg)
background-size: 350px
background-position: 80% 80%

# 2. Papo inicial

## Histórico - Linguagem R

### Robert Gentleman e Ross Ihaka (Universidade de Aucland, NZ)
- Desenvolvimento (1997-2000)
- Versão 1 (2000-2004)
- Versão 2 (2004-2013)
- Versão 3 (2013-2020)
- Versão 4 (2020-????)
<br><br>
- Interface: RStudio (2011-atual)
- Atualmente: **R Core Team**

<br><br><br>

[http://vita.had.co.nz/papers/r-s.pdf]

---

background-image: url(img/general_hard_soft.png)
background-size: 800px
background-position: 50% 50%

# Sempre bom lembrar

---

background-image: url(img/r_site.png)
background-size: 600px
background-position: 50% 50%

# 3. Instalando o R

## R-Project

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://www.r-project.org/

---

background-image: url(img/ubuntu.png)
background-size: 130px
background-position: 90% 90%

# 3. Instalando o R

## R-Project - R 4.0 no Ubuntu 20.04

```{bash,eval=FALSE}
# add key
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9

# add ppa
echo -e "\ndeb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/" | sudo tee -a /etc/apt/sources.list

# update source list
sudo apt update

# install r
sudo apt install -y r-base-core  # r
sudo apt install -y r-base-dev   # devtools
```

---

class: inverse, middle, center

# 4. Instalando o RStudio

---

background-image: url(img/rstudio_site.png)
background-size: 600px
background-position: 50% 50%

# 4. Instalando o RStudio

## RStudio

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://rstudio.com/

---

background-image: url(img/ubuntu.png)
background-size: 130px
background-position: 90% 90%

# 4. Instalando o RStudio

## RStudio - Ubuntu*

```{bash,eval=FALSE}
# download
wget -c https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.2.5042-amd64.deb

# install
sudo dpkg -i rstudio-1.2.5042-amd64.deb

# install dependencies
sudo apt install -fy

# remove .deb file
rm rstudio-1.2.5042-amd64.deb
```

---

background-image: url(img/r_vs_rstudio.png)
background-size: 600px
background-position: 50% 80%

# 4. Instalando o RStudio

## Todos conhecem o RGui e o RStudio?

---

background-image: url(img/r_vs_rstudio2.png),url(img/r_logo.svg),url(img/rstudio_logo.svg)
background-size: 600px,150px,250px
background-position: 50% 80%,28% 30%,73% 35%

# 4. Instalando o RStudio

## R vs RStudio

---

class: clear, inverse, middle, center

# 5. Como o RStudio funciona?

---

background-image: url(img/rstudio_working.png)
background-size: 750px
background-position: 50% 70%

# 5. Como o RStudio funciona?

## Comandos

---

background-image: url(img/rstudio_console.png)
background-size: 800px
background-position: 50% 70%

# 5. Como o RStudio funciona?

## Estrutura da IDE

---

background-image: url(img/rstudio_console_working.png)
background-size: 800px
background-position: 50% 70%

# 5. Como o RStudio funciona?

## Funcionamento

---

class: clear, inverse, middle, center

# 6. Pacotes

---

background-image: url(img/package_r.png)
background-size: 300px
background-position: 50% 80%

# 6. Pacotes

## Pacotes são **coleções de função** para fazer análises ou manejar tipos de **dados específicos**

---

background-image: url(img/person_rafael.jpg),url(img/package_geobr2.png)
background-size: 170px,250px
background-position: 5% 75%,80% 90%

# 7. Instalando meu primeiro pacote

## Instalando o pacote
```{r,eval=FALSE}
install.packages("geobr")
```

## Carregando o pacote
```{r}
library(geobr)
```

<br><br><br><br><br>

#### Rafael Pereira

[*] https://github.com/ipeaGIT/geobr

---

# 7. Instalando meu primeiro pacote

## Download dos dados
```{r,message=FALSE,warning=FALSE}
# download dos dados
estados_brasil <- geobr::read_state(code_state = "all",
                                    year = 2018,
                                    simplified = TRUE,
                                    showProgress = FALSE)
estados_brasil
```

---

# 7. Instalando meu primeiro pacote

## Plot simples
```{r,fig.align='center'}
# mapa dos estados
plot(estados_brasil[2])
```

---

background-image: url(img/package_spatial.jpg)
background-size: 400px
background-position: 50% 70%

# Pacotes para análises geoespaciais

---

class: inverse, middle, center

# Principal material para estudo

---

background-image: url(img/cover_ciencias_dados_r.jpg)
background-size: 300px
background-position: 50% 50%

# 8. Material

## Ciência de Dados com R: introdução (2018)

<br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://www.ibpad.com.br/o-que-fazemos/publicacoes/introducao-ciencia-de-dados-com-r/

---

background-image: url(img/cover_analise_espacial_r.png)
background-size: 300px
background-position: 50% 55%

# 8. Material

## Análise espacial com R (2019)

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://www.dropbox.com/s/blgtp2bmpdghol7/AnaliseEspacialComR.pdf

---

background-image: url(img/cover_data_science_r.png)
background-size: 300px
background-position: 50% 55%

# 8. Material 

## R for Data Science (2017)

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://r4ds.had.co.nz/

---

background-image: url(img/cover_statistical_inference.png)
background-size: 300px
background-position: 50% 55%

# 8. Material 

## Statistical Inference via Data Science (2019)

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://moderndive.com

---

background-image: url(img/cover_rmarkdown.png)
background-size: 280px
background-position: 50% 50%

# 8. Material 

## R Markdown: The Definitive Guide (2018)

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://bookdown.org/yihui/rmarkdown/

---

background-image: url(img/cover_geocompr.png)
background-size: 280px
background-position: 50% 50%

# 8. Material 

## Geocomputation with R (2019)

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://geocompr.robinlovelace.net/

---

background-image: url(img/cover_geospatial_health_data.jpg)
background-size: 280px
background-position: 50% 55%

# 8. Material 

## Geospatial Health Data (2019)

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] http://www.paulamoraga.com/book-geospatial/

---

class: clear
background-image: url(img/logo_werlive.png)
background-size: 400px
background-position: 90% 60%

## Maurício Vancine

`r icon::fa_envelope()` [mauricio.vancine@gmail.com](mauricio.vancine@gmail.com)
<br>
`r icon::fa_twitter()` [mauriciovancine](https://twitter.com/mauriciovancine)
<br>
`r icon::fa_github()` [@mauriciovancine](https://mauriciovancine.netlify.com/)
<br>
`r icon::fa_link()` [mauriciovancine.netlify.com](https://mauriciovancine.netlify.com/)

<br><br>

## Felipe Sodré Barros
`r icon::fa_envelope()` [felipe.b4rros@gmail.com](felipe.b4rros@gmail.com)
<br>
`r icon::fa_twitter()` [@FelipeSMBarros](https://twitter.com/FelipeSMBarros)
<br>
`r icon::fa_gitlab()` [@felipe.b4rros](https://gitlab.com/felipe.b4rros")
<br>
`r icon::fa_link()` [Geo Independência ](https://geoind.wordpress.com/)

<br><br>

Slides criados via pacote [xaringan](https://github.com/yihui/xaringan) e tema [Metropolis](https://github.com/pat-s/xaringan-metropolis)