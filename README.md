# Lives de R
Repositório criado para as lives da linguagem [R](https://www.r-project.org/) do canal [GeoCastBrasil](https://www.youtube.com/channel/UCLAeX4dyujMoy4xqHvxSDpQ).  

## Sobre
Este material está sendo elaborado por [Maurício Vancine](https://mauriciovancine.netlify.com/) e [Felipe Sodré Barros]() como uma forma de organizar os conteúdos das lives da linguagem R. 

Buscaremos manter um único estilo de programação (*coding style*) sugeridos pelo [Hadley Wickham](http://adv-r.had.co.nz/Style.html) e [Yihui Xie](https://yihui.org/formatr/). Usaremos também a versão mais recente do [The tidyverse style guide](https://style.tidyverse.org/).

A cada live, terá uma hora de duração (aproximadamente :), dividido em 15/20 min. de introdução, comentários sobre o que será apresentado; 30/40 min. de "mão na massa" e os minutos que sobrarem para considerações finais.

Deixaremos listado neste README os *links* para os principais passos/processos executados, como forma de facilitar a busca de conteúdos. 

Com relação aos dados usados, tentaremos ao máximo, usar dados abertos e de fácil acesso, focando em funções para baixar os dados utilizando a própria linguagem. 

Caso tenham dúvidas, sugestões de temas a serem explorados ou correções em nossos materiais, nos avise [abrindo uma issue](https://docs.gitlab.com/ee/user/project/issues/) neste repositório, ou vejam [como contribuir](README.md#como-contribuir). 

Caso queira saber mais sobre git/gitlab, [veja este link](https://medium.com/ekode/primeiros-passos-com-git-e-gitlab-criando-seu-primeiro-projeto-89f9001614b0).  

## Informações técnicas
Iremos utilizar o [R](https://www.r-project.org/) versão 4.0 e o [RStudio](https://rstudio.com/), no Sistema Operacional (SO) Linux, como o [Ubuntu](https://ubuntu.com/) e [Linux Mint](https://www.linuxmint.com/), entretando, esses programas também funcionam em outros SOs como Windows ou MacOS.

## Breve introdução à linguagem R
Caso você nunca tenha programado em R, considere dar uma olhada antes nesse [material em português](https://www.curso-r.com/material/) e nesse outro [material em inglês](https://education.rstudio.com/learn/).

## Lives
Listamos a seguir as lives realizadas:  

### Live 1: ["Por onde começaR?"](https://www.youtube.com/watch?v=ZORFVdwtJ1U)
1. [Ideia das lives de R - We R Live]()
2. [Papo inicial]()
3. [Instalando o R]()
4. [Instalando o RStudio]()
5. [Como o RStudio funciona?]()
6. [Pacotes]()
7. [Instalando nosso primeiro pacote]()
8. [Material (mas não muito)]()

Vídeo: https://youtu.be/ZORFVdwtJ1U
<br>
Apresentação: [bitly/werlive01](https://mauriciovancine.netlify.app/slides/werlive_01/werlive_01)

### Live 2: ["VetoRes e Mapas usando ggplot2 no R"](https://www.youtube.com/watch?v=eHht0n3Ppcg)
15/20 min: Introdução
1. [Desafio da live 2]()
2. [Pacotes a serem usados (instalação)]()
3. [Pequenas considerações sobre a sintaxe]()
30 min: Mão na massa
4. [Carregar dados vetoriais]()
5. [Mapas usando ggplot2]()
6. [Considerações finais]()

Vídeo: https://youtu.be/eHht0n3Ppcg
<br>
Apresentação: [bitly/werlive02](https://mauriciovancine.netlify.app/slides/werlive_02/werlive_02)
Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/werlive_02/werlive_02.R

## Mais conteúdos
### Sites
* [Curso-R](https://www.curso-r.com/)  
* [Finding Your Way To R](https://education.rstudio.com/learn/)

### Apostilas
* [Introdução ao software estatístico R](http://professor.ufrgs.br/sites/default/files/matiasritter/files/apostila_introducao_ao_r_-_ritter_they_and_konzen.pdf)

### Livros
#### Português
* [Ciência de dados com R](https://cdr.ibpad.com.br/index.html)
* [Software R: Análise estatísticade dados utilizando umprograma livre](http://www.editorafaith.com.br/ebooks/grat/software_r.pdf)
* [R pragmático](https://curso-r.github.io/ragmatic-book)
* [Análise espacial com R](https://www.dropbox.com/s/blgtp2bmpdghol7/AnaliseEspacialComR.pdf?dl=0)

#### Inglês
* [R for Data Science](https://r4ds.had.co.nz/)
* [Advanced R](https://adv-r.hadley.nz/)
* [Hands-On Programming with R](https://rstudio-education.github.io/hopr/)
* [Efficient R programming](https://bookdown.org/csgillespie/efficientR/)
* [R Programming for Data Science](https://bookdown.org/rdpeng/rprogdatascience/)
* [Statistical Inference via Data Science](https://moderndive.com/)
* [R Graphics Cookbook](https://r-graphics.org/)
* [Fundamentals of Data Visualization](https://serialmentor.com/dataviz/)
* [Text Mining with R](https://www.tidytextmining.com/)
* [R Markdown: The Definitive Guide](https://bookdown.org/yihui/rmarkdown/)
* [Geocomputation with R](https://geocompr.robinlovelace.net/)
* [Geospatial Health Data: Modeling and Visualization with R-INLA and Shiny](http://www.paulamoraga.com/book-geospatial/)
* [The R inferno](https://www.burns-stat.com/pages/Tutor/R_inferno.pdf) :smiling_imp:

#### Cheatsheets
[RStudio Cheatsheets](https://rstudio.com/resources/cheatsheets/)

---

## Venha trocar umas ideias
[YouTube](https://www.youtube.com/channel/UCLAeX4dyujMoy4xqHvxSDpQ)

[Instagram](https://www.instagram.com/geocastbrasil)

[Twitter](https://twitter.com/GeoCastBrasil)

[Facebook](https://www.facebook.com/GeoCastBrasil/)

## Como contribuir
Como boas práticas, faça o *fork* desse repositório, faça as modificações sugeridas e abrar um *pull request*. 
